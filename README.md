# Toy Robot

Toy Robot Coding Challenge

## Description
This Toy Robot Coding Challenge is implemented using Python

## Running the Code
In the terminal, use this command: python main.py

## Running the test
In main.py comment these lines:
#
filename = 'toy_robot_input_file'

print('TOY ROBOT CODE CHALLENGE')

toy_robot = main(filename)
#

Proceed to test folder and open test_toy_robot and run the test