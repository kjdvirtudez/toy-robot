from re import compile, X

from toy_robot.position import Position
from toy_robot.robot_movements import RobotMovements
from toy_robot.table import Table


def main(file):
    pattern = compile(
        r"""
            (?<=^)                                          # search for the match of the start of string
            (?P<cmd>MOVE$|LEFT$|RIGHT$|REPORT$|PLACE        # search for items within the group name "cmd"
            (?=\s?                                          # space
            (?P<x>\d+),                                     # search for the value of x coordinates
            (?P<y>\d+),                                     # search for the value of y coordinates
            (?P<dir>NORTH|EAST|SOUTH|WEST)                  # search for items within the group name "dir"
            $))                                             # EOL
        """,
        X
    )

    direction_list = ["NORTH", "EAST", "SOUTH", "WEST"]

    table = Table(Position(0, 0), Position(4, 4))
    robot = RobotMovements(direction=direction_list)
    output = ''

    with open(file) as f:
        for line in f:
            result = pattern.match(line.rstrip('\n'))

            if result and result.group("cmd").lower() == "place":
                robot = robot.set_placement(
                    position=Position(float(result.group("x")), float(result.group("y"))),
                    idx=direction_list.index(result.group("dir")),
                    table=table
                )
            elif result and robot.table:
                if result.group("cmd").lower() == "left":
                    robot.move_left()
                elif result.group("cmd").lower() == "right":
                    robot.move_right()
                elif result.group("cmd").lower() == "move":
                    robot.apply_movement()
                elif result.group("cmd").lower() == "report":
                    output = robot.report_new_placement()

    return output


filename = 'toy_robot_input_file'
print('TOY ROBOT CODE CHALLENGE')
toy_robot = main(filename)

