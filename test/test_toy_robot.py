import unittest

from main import main


class TestToyRobot(unittest.TestCase):
    def test_success_toy_robot_north(self):
        print('=====TEST FOR TOY ROBOT MOVING NORTH=====')
        filename = 'toy_robot_north_input_file'
        output = main(filename)

        self.assertEqual(int(output.position.x), 0, 'Incorrect X Value')
        self.assertEqual(int(output.position.y), 0, 'Incorrect Y Value')
        self.assertEqual(output.direction[output.idx], 'WEST', 'Incorrect Facing Value')

    def test_success_toy_robot_east(self):
        print('=====TEST FOR TOY ROBOT MOVING EAST=====')
        filename = 'toy_robot_east_input_file'
        output = main(filename)

        self.assertEqual(int(output.position.x), 3, 'Incorrect X Value')
        self.assertEqual(int(output.position.y), 3, 'Incorrect Y Value')
        self.assertEqual(output.direction[output.idx], 'NORTH', 'Incorrect Facing Value')

    def test_success_toy_robot_south(self):
        print('=====TEST FOR TOY ROBOT MOVING SOUTH=====')
        filename = 'toy_robot_south_input_file'
        output = main(filename)

        self.assertEqual(int(output.position.x), 2, 'Incorrect X Value')
        self.assertEqual(int(output.position.y), 0, 'Incorrect Y Value')
        self.assertEqual(output.direction[output.idx], 'SOUTH', 'Incorrect Facing Value')

    def test_success_toy_robot_west(self):
        print('=====TEST FOR TOY ROBOT MOVING WEST=====')
        filename = 'toy_robot_west_input_file'
        output = main(filename)

        self.assertEqual(int(output.position.x), 0, 'Incorrect X Value')
        self.assertEqual(int(output.position.y), 1, 'Incorrect Y Value')
        self.assertEqual(output.direction[output.idx], 'SOUTH', 'Incorrect Facing Value')

    def test_toy_robot_out_of_the_table(self):
        print('=====TEST FOR TOY ROBOT WITH OUT OF THE TABLE====')
        filename = 'toy_robot_out_of_table_input_file'
        output = main(filename)

        self.assertEqual(int(output.position.x), 3, 'Incorrect X Value')
        self.assertEqual(int(output.position.y), 4, 'Incorrect Y Value')
        self.assertEqual(output.direction[output.idx], 'NORTH', 'Incorrect Facing Value')

    def test_toy_robot_with_extra_cmd_before_place(self):
        print('=====TEST FOR TOY ROBOT WITH EXTRA COMMANDS BEFORE PLACE====')
        filename = 'toy_robot_extra_cmd_input_file'
        output = main(filename)

        self.assertEqual(int(output.position.x), 0, 'Incorrect X Value')
        self.assertEqual(int(output.position.y), 1, 'Incorrect Y Value')
        self.assertEqual(output.direction[output.idx], 'NORTH', 'Incorrect Facing Value')

