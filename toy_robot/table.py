
class Table:

    def __init__(self, min_table_location, max_table_location):
        self.min_table_location = min_table_location
        self.max_table_location = max_table_location

    def check_if_movement_is_within_table_size(self, location):

        return self.min_table_location.less_than_current_position(location) and \
               self.max_table_location.greater_than_current_position(location)
