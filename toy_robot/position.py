
class Position:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def increment_x(self):

        self.x += 1

    def decrement_x(self):

        self.x -= 1

    def increment_y(self):

        self.y += 1

    def decrement_y(self):

        self.y -= 1

    def greater_than_current_position(self, new_location):

        return self.x >= new_location.x and self.y >= new_location.y

    def less_than_current_position(self, new_location):

        return self.x <= new_location.x and self.y <= new_location.y
