from toy_robot.position import Position


MAX_TABLE_POSITION = 4


class RobotMovements:

    def __init__(self, position=Position(0.0, 0.0), idx=0, table=None, direction=None):
        self.position = position
        self.idx = idx
        self.table = table
        self.direction = direction

    def set_placement(self, position, idx, table):
        if table and table.check_if_movement_is_within_table_size(position):

            return RobotMovements(position, idx, table, self.direction)
        else:
            self.revert_placement(position)

            return self

    def move_left(self):
        self.idx = (self.idx - 1) % len(self.direction)

        return self.set_placement(self.position, self.idx, self.table)

    def move_right(self):
        self.idx = (self.idx + 1) % len(self.direction)

        return self.set_placement(self.position, self.idx, self.table)

    def apply_movement(self):
        if self.direction[self.idx].lower() == 'north':
            self.position.increment_y()
        elif self.direction[self.idx].lower() == 'east':
            self.position.increment_x()
        elif self.direction[self.idx].lower() == 'south':
            self.position.decrement_y()
        elif self.direction[self.idx].lower() == 'west':
            self.position.decrement_x()

        return self.set_placement(self.position, self.idx, self.table)

    def report_new_placement(self):
        if self.table:
            print(f'Output: {round(self.position.x)}, {round(self.position.y)}, {self.direction[self.idx]}')

        return self

    def revert_placement(self, position):
        if position.x == MAX_TABLE_POSITION + 1:
            position.decrement_x()

        if position.y == MAX_TABLE_POSITION + 1:
            position.decrement_y()
